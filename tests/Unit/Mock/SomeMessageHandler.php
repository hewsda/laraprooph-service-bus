<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit\Mock;

class SomeMessageHandler
{
    private $lastMessage;

    private $invokeCounter = 0;

    public function __invoke($message): void
    {
        $this->lastMessage = $message;
        $this->invokeCounter++;
    }

    public function handle($message): void
    {
        $this->lastMessage = $message;
        $this->invokeCounter++;
    }

    public function onEvent($message): void
    {
        $this->lastMessage = $message;
        $this->invokeCounter++;
    }

    public function getInvokeCounter(): int
    {
        return $this->invokeCounter;
    }

    public function getLastMessage()
    {
        return $this->lastMessage;
    }
}