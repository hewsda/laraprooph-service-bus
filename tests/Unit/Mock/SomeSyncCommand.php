<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit\Mock;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class SomeSyncCommand extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function createCommand(string $data): SomeSyncCommand
    {
        return new self([
            'data' => $data,
        ]);
    }
}