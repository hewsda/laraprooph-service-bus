<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit;

use Laraprooph\ServiceBus\CommandBusManager;
use LaraproophTests\ServiceBus\TestCase;
use Prooph\ServiceBus\MessageBus;

class MessageBusManagerTest extends TestCase
{
    private $bus;

    public function setUp()
    {
        $this->bus = $this->getMockForAbstractClass(MessageBus::class);
    }

    /**
     * @test
     */
    public function it_can_return_service_bus_instance()
    {
        $m = $this->getMockBus();

        $m->method('getDefaultDriver')->willReturn('foo');
        $m->method('getDefaultRoutes')->willReturn([]);
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);

        $this->assertSame($this->bus, $m->message());
    }

    /**
     * @test
     */
    public function it_can_return_the_same_instance()
    {
        $m = $this->getMockBus();
        $m->method('getDefaultDriver')->willReturn('foo');
        $m->method('getDefaultRoutes')->willReturn([]);
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);

        $this->assertSame($this->bus, $m->message());
    }

    /**
     * @test
     */
    public function it_return_default_service_bus_instance_when_no_bus_provided()
    {
        $m = $this->getMockBus();
        $m->method('getDefaultDriver')->willReturn('some_bus');
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);
        $m->method('getDefaultRoutes')->willReturn([]);

        $this->assertSame($this->bus, $m->message());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_raise_exception_when_no_bus_configured()
    {
        $m = $this->getMockBus();
        $m->method('getDefaultDriver')->willReturn(null);
        $m->message();
        $m->message('any');
    }

    public function getMockBus(): \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockBuilder(CommandBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods([ 'resolve', 'getDefaultDriver', 'getConfig', 'getDefaultRoutes'])
            ->getMock();
    }
}