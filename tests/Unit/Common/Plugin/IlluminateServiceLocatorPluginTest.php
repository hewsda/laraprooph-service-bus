<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit\Common\Plugin;

use Illuminate\Contracts\Container\Container;
use Laraprooph\ServiceBus\Common\Container\IlluminateContainerAware;
use Laraprooph\ServiceBus\Common\Plugin\IlluminateServiceLocatorPlugin;
use LaraproophTests\ServiceBus\TestCase;
use LaraproophTests\ServiceBus\Unit\Mock\SomeMessageHandler;
use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Plugin\ServiceLocatorPlugin;

class IlluminateServiceLocatorPluginTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_locate_service_using_decorated_illuminate_container()
    {
        $handler = new SomeMessageHandler();

        $illContainer = $this->prophesize(Container::class);
        $illContainer->bound('custom-handler')->willReturn(true)->shouldBeCalled();
        $illContainer->make('custom-handler')->willReturn($handler)->shouldBeCalled();

        $commandBus = new CommandBus();

        $plugin = new IlluminateServiceLocatorPlugin(new ServiceLocatorPlugin(
            new IlluminateContainerAware($illContainer->reveal())
        ));
        $plugin->attachToMessageBus($commandBus);

        $commandBus->attach(CommandBus::EVENT_DISPATCH, function (ActionEvent $actionEvent): void {
            $actionEvent->setParam(CommandBus::EVENT_PARAM_MESSAGE_HANDLER, 'custom-handler');
        },
            CommandBus::PRIORITY_INITIALIZE
        );

        $commandBus->dispatch('foo');
    }
}