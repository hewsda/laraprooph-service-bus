<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit\Common\Container;

use Illuminate\Contracts\Container\Container;
use Laraprooph\ServiceBus\Common\Container\IlluminateContainerAware;
use LaraproophTests\ServiceBus\TestCase;
use LaraproophTests\ServiceBus\Unit\Mock\SomeInterface;

class IlluminateContainerAwareTest extends TestCase
{
    /**
     * @test
     */
    public function it_can_be_constructed_with_illuminate_container()
    {
        $m = $this->getMockBuilder(Container::class)->getMock();

        $ins = new IlluminateContainerAware($m);

        $this->assertInstanceOf(IlluminateContainerAware::class, $ins);
    }

    /**
     * @test
     */
    public function it_can_check_if_service_id_exists()
    {
        $m = $this->getMockBuilder(Container::class)->getMock();
        $m->expects($this->once())->method('bound')->willReturn(true);
        $ins = new IlluminateContainerAware($m);

        $this->assertTrue($ins->has('any'));
    }

    /**
     * @test
     */
    public function it_can_resolve_a_service_id()
    {
        $m = $this->getMockBuilder(Container::class)->getMock();
        $m->expects($this->once())->method('make')->willReturn('foo');
        $m->expects($this->once())->method('bound')->willReturn(true);

        $ins = new IlluminateContainerAware($m);
        $bar = $ins->get('any');

        $this->assertEquals('foo', $bar);
    }

    /**
     * @test
     * @expectedException \Laraprooph\ServiceBus\Common\Exception\ContainerAliasNotFoundException
     */
    public function it_raise_exception_when_service_id_does_not_exists()
    {
        $m = $this->getMockBuilder(Container::class)->getMock();
        $m->expects($this->once())->method('bound')->willReturn(false);
        $ins = new IlluminateContainerAware($m);

        $ins->get('any');
    }

    /**
     * @test
     * @expectedException \Laraprooph\ServiceBus\Common\Exception\ContainerBindingException
     */
    public function it_raise_exception_when_service_can_not_be_resolved()
    {
        $ci = new \Illuminate\Container\Container();
        $ci->bind('bar', SomeInterface::class);

        $c = new IlluminateContainerAware($ci);
        $c->get('bar');
    }

    /**
     * @test
     */
    public function it_raise_exception_when_service_can_not_be_found()
    {
        $this->markTestSkipped('Todo:' . __FUNCTION__);
    }
}