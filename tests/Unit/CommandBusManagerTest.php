<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit;

use Laraprooph\ServiceBus\CommandBusManager;
use LaraproophTests\ServiceBus\TestCase;
use Prooph\ServiceBus\MessageBus;

class CommandBusManagerTest extends TestCase
{
    private $bus;

    public function setUp()
    {
        $this->bus = $this->getMockForAbstractClass(MessageBus::class);
    }

    /**
     * @test
     */
    public function it_can_return_message_bus_instance()
    {
        $m = $this->getMockBus();

        $m->method('getDefaultDriver')->willReturn('foo');
        $m->method('getDefaultRoutes')->willReturn([]);
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);

        $this->assertSame($this->bus, $m->message());
    }

    /**
     * @test
     */
    public function it_can_return_the_same_instance()
    {
        $m = $this->getMockBus();
        $m->method('getDefaultDriver')->willReturn('foo');
        $m->method('getDefaultRoutes')->willReturn([]);
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);

        $this->assertSame($this->bus, $m->message());
    }

    /**
     * @test
     */
    public function it_return_default_service_bus_instance_when_no_bus_provided()
    {
        $m = $this->getMockBus();
        $m->method('getDefaultDriver')->willReturn('some_bus');
        $m->expects($this->once())->method('resolve')->willReturn($this->bus);
        $m->method('getDefaultRoutes')->willReturn([]);

        $this->assertSame($this->bus, $m->message());
    }

    /**
     * @test
     */
    public function it_return_bus_instance_configured()
    {
        $m = $this->getMockBus();

        $m->method('getDefaultDriver')->willReturn('some_bus');

        $m->method('getDefaultRoutes')->willReturn([]);

        $m->method('getOptions')->willReturn($this->optionsConfig());


        $this->assertEquals('some_bus',$m->message());
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_raise_exception_when_no_bus_name_provided()
    {
        $m = $this->getMockBus();
        $m->method('getDefaultDriver')->willReturn(null);
        $m->message();
    }

    public function getMockBus(): \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockBuilder(CommandBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['resolve', 'getDefaultDriver', 'getConfig', 'getDefaultRoutes',
                'createBus', 'getOptions', 'attachRouter', 'registerBusPlugins', 'registerRoutes'])
            ->getMock();
    }

    public function getBusConfig()
    {
        return [
            'concrete' => \Prooph\ServiceBus\CommandBus::class,
            'router' => [
                'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class,
            ]
        ];
    }

    public function optionsConfig()
    {
        return [
            'enable_handler_location' => true,
            'message_factory' => \Prooph\Common\Messaging\MessageFactory::class,
            'plugins' => []
        ];
    }
}

