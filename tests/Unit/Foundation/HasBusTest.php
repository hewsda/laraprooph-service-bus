<?php

declare(strict_types=1);

namespace LaraproophTests\ServiceBus\Unit\Foundation;

use Illuminate\Container\Container;
use Illuminate\Foundation\Application;
use Laraprooph\ServiceBus\CommandBusManager;
use Laraprooph\ServiceBus\Foundation\HasBus;
use LaraproophTests\ServiceBus\TestCase;
use LaraproophTests\ServiceBus\Unit\Mock\SomeSyncCommand;
use Prooph\ServiceBus\MessageBus;

class HasBusTest extends TestCase
{
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy
     */
    private $bus;

    public function setUp()
    {
        $this->bus = $this->getMockForAbstractClass(MessageBus::class);
    }

    /**
     * @test
     */
    public function it_can_dispatch_command_with_default_bus()
    {
        $this->markTestSkipped('not accurate');
        $app = new Application();
        $c = new Container();
        $app::setInstance($c);

        $m = $this->getMockBuilder(CommandBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['command'])
            ->getMock();

        $m->expects($this->once())->method('command')->willReturn($this->bus);

        $c->instance('bus_manager.command', $m);

        $dis = new SomeClass();

        $command = SomeSyncCommand::createCommand('some message');
        $dis->dispatchSomeCommand($command);
    }
}

class SomeClass
{
    use HasBus;

    public function dispatchSomeCommand($command)
    {
        $this->dispatchCommand($command);
    }
}
