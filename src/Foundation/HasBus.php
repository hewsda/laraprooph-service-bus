<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Foundation;

trait HasBus
{
    protected function dispatchCommand($command, string $busName = null): void
    {
        if ($busName) {
            app($busName)->dispatch($command);
            return;
        }

        app('bus_manager.command')->command()->dispatch($command);
    }

    protected function dispatchQuery($query, string $busName = null)
    {
        if ($busName) {
            return app($busName)->dispatch($query);
        }

        return app('bus_manager.query')->query($busName)->dispatch($query);
    }

    protected function dispatchEvent($event, string $busName = null): void
    {
        if ($busName) {
            app($busName)->dispatch($event);
            return;
        }

        app('bus_manager.event')->event($busName)->dispatch($event);
    }
}