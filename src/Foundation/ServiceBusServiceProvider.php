<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Foundation;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Laraprooph\ServiceBus\CommandBusManager;
use Laraprooph\ServiceBus\EventBusManager;
use Laraprooph\ServiceBus\QueryBusManager;

class ServiceBusServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function boot(): void
    {
        $this->publishes(
            [
                __DIR__ . '/../../config/laraprooph_message_bus.php' => config_path('laraprooph.php')
            ],
            'laraprooph');
    }

    public function register(): void
    {
        $this->registerConfig();

        $this->registerMessageBus();
    }

    public function registerConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/laraprooph_message_bus.php', 'laraprooph');
    }

    public function registerMessageBus(): void
    {
        $this->app->singleton('bus_manager.command', function (Application $app) {
            return new CommandBusManager($app);
        });

        $this->app->singleton('bus_manager.event', function (Application $app) {
            return new EventBusManager($app);
        });

        $this->app->singleton('bus_manager.query', function (Application $app) {
            return new QueryBusManager($app);
        });
    }

    public function provides(): array
    {
        return [
            'bus_manager.command',
            'bus_manager.event',
            'bus_manager.query'
        ];
    }
}