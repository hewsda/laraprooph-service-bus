<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class EventBusManager extends MessageBusManager
{
    public function event(string $name = null, array $routes = []): MessageBus
    {
        return parent::message($name, $routes);
    }

    protected function for (): string
    {
        return 'event';
    }
}