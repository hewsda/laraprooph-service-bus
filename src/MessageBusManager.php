<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Illuminate\Contracts\Foundation\Application;
use Laraprooph\ServiceBus\Common\Container\IlluminateContainerAware;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\Router\AsyncSwitchMessageRouter;
use Prooph\ServiceBus\Plugin\ServiceLocatorPlugin;
use Psr\Container\ContainerInterface;

abstract class MessageBusManager
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $messageBus = [];

    /**
     * MessageBusManager constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function message(string $name = null, array $routes = []): MessageBus
    {
        $serviceId = $name ?? $this->getDefaultDriver();

        if (!$serviceId) {
            throw new \InvalidArgumentException(
                sprintf('No driver provided for %s bus manager.', $this->for())
            );
        }

        if (isset($this->messageBus[$serviceId])) {
            return $this->messageBus[$serviceId];
        }

        $routes = $routes ?: $this->getDefaultRoutes($serviceId);

        return $this->messageBus[$serviceId] = $this->resolve($serviceId, $routes);
    }

    protected function resolve(string $serviceId, array $routes): MessageBus
    {
        $config = $this->getConfig($serviceId);

        if (!$config) {
            throw new \InvalidArgumentException(
                sprintf('Service bus id %s not found in config.', $serviceId)
            );
        }

        return $this->createBus($serviceId, $config, $routes);
    }

    protected function createBus(string $alias, array $config, array $routes): MessageBus
    {
        $instance = new $config['concrete'];

        $options = $this->getOptions($config);

        $plugins = $options['plugins'];

        if (true === $options['enable_handler_location']) {
            $plugins[] = $this->registerServiceLocator();
        }

        $this->registerBusPlugins($instance, $plugins);

        $this->attachRouter($instance, $config['router'], $routes);

        if ($options['auto_bind_routes']) {
            $this->registerRoutes($routes);
        }

        return $instance;
    }

    protected function registerBusPlugins(MessageBus $bus, array $plugins): void
    {
        foreach ($plugins as $plugin) {
            $this->app->bindIf($plugin);

            $this->app[$plugin]->attachToMessageBus($bus);
        }
    }

    protected function attachRouter(MessageBus $bus, array $configRouter, array $routes): void
    {
        $router = new $configRouter['concrete']($routes);

        if (isset($configRouter['async_switch'])) {
            $asyncMessageProducer = $this->app->make($configRouter['async_switch']);

            $router = new AsyncSwitchMessageRouter($router, $asyncMessageProducer);
        }

        $router->attachToMessageBus($bus);
    }

    protected function registerServiceLocator(): string
    {
        $this->app->bind(ContainerInterface::class, IlluminateContainerAware::class);

        $this->app->bind(ServiceLocatorPlugin::class);

        return ServiceLocatorPlugin::class;
    }

    protected function registerRoutes(array $routes): void
    {
        if ('event' === $this->for()) {

            foreach ($routes as $event => $handlers) {
                foreach ($handlers as $handler) {
                    $this->app->bindIf($handler);
                }
            }
            return;
        }

        foreach ($routes as $command => $handler) {
            $this->app->bindIf($command);
            $this->app->bindIf($handler);
        }
    }

    protected function getOptions(array $config): array
    {
        $default = $this->app['config']->get('laraprooph.bus_manager.options');

        if (!$default) {
            $default = [];
        }

        return array_merge($default, $config['options'] ?? []);
    }

    protected function getDefaultRoutes(string $name): array
    {
        $routes = $this->app['config']->get('laraprooph.service_bus.' . $name . '.router.routes');

        return !empty($routes) ? $routes : [];
    }

    protected function getDefaultDriver(): ?string
    {
        return $this->app['config']->get('laraprooph.bus_manager.defaults.' . $this->for());
    }

    protected function getConfig(string $name): ?array
    {
        return $this->app['config']->get('laraprooph.bus_manager.service_bus.' . $this->for() . '.' . $name);
    }

    abstract protected function for (): string;
}