<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class CommandBusManager extends MessageBusManager
{
    public function command(string $name = null, array $routes = []): MessageBus
    {
        return parent::message($name, $routes);
    }

    protected function for (): string
    {
        return 'command';
    }
}