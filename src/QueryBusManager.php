<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class QueryBusManager extends MessageBusManager
{
    public function query(string $name = null, array $routes = []): MessageBus
    {
        return parent::message($name, $routes);
    }

    protected function for (): string
    {
        return 'query';
    }
}