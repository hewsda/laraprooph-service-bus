<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Common\Exception;

use Psr\Container\NotFoundExceptionInterface;

class ContainerAliasNotFoundException extends \RuntimeException implements NotFoundExceptionInterface
{

}