<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Common\Exception;

use Psr\Container\ContainerExceptionInterface;

class ContainerBindingException extends \RuntimeException implements ContainerExceptionInterface
{
}