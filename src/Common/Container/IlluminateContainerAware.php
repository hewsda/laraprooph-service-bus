<?php

declare(strict_types=1);

namespace Laraprooph\ServiceBus\Common\Container;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Container\Container;
use Laraprooph\ServiceBus\Common\Exception\ContainerAliasNotFoundException;
use Laraprooph\ServiceBus\Common\Exception\ContainerBindingException;
use Psr\Container\ContainerInterface;

class IlluminateContainerAware implements ContainerInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * IlluminateContainerAware constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function get($id)
    {
        if (!$this->has($id)) {
            throw new ContainerAliasNotFoundException(
                sprintf('Alias with id %s not found.', $id)
            );
        }

        try {
            return $this->container->make($id);
        } catch (BindingResolutionException $exception) {
            throw new ContainerBindingException($exception->getMessage(), 0, $exception);
        }
    }

    public function has($id): bool
    {
        return $this->container->bound($id);
    }
}