<?php

return [
    'bus_manager' => [

        'defaults' => [
            'command' => 'command_bus',
            'event' => 'event_bus',
            'query' => 'query_bus'
        ],

        /**
         * Service bus
         *
         * Each bus should provide an alias and a concrete router
         * Router routes are provided by any service provider
         */
        'service_bus' => [

            'command' => [
                'command_bus' => [
                    'concrete' => \Prooph\ServiceBus\CommandBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class,
                    ]
                ],
            ],

            'event' => [
                'event_bus' => [
                    'concrete' => \Prooph\ServiceBus\EventBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\EventRouter::class,
                    ],
                    'options' => [
                        'plugins' => [
                            \Prooph\ServiceBus\Plugin\InvokeStrategy\OnEventStrategy::class,
                        ],
                    ]
                ]
            ],

            'query' => [
                'query_bus' => [
                    'concrete' => \Prooph\ServiceBus\QueryBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\QueryRouter::class,

                    ],
                ]
            ]
        ],

        /**
         *  Global options for each bus
         *
         *  Set options in each bus config to override it
         */
        'options' => [
            'enable_handler_location' => true,
            'message_factory' => \Prooph\Common\Messaging\MessageFactory::class,
            'plugins' => [],
            'auto_bind_routes' => true
        ]
    ],

    'service_bus' => [

        'command_bus' => [
            'router' => [
                'routes' => [],
            ],
        ],

        'event_bus' => [
            'router' => [
                'routes' => [],
            ],
        ],

        'query_bus' => [
            'router' => [
                'routes' => [],
            ],
        ],
    ]
];
